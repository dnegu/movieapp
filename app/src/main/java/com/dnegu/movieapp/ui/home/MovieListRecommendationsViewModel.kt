package com.dnegu.movieapp.ui.home

import com.dnegu.core.common.onFailure
import com.dnegu.core.common.onSuccess
import com.dnegu.core.movie.GetMovieListRecommendations
import com.dnegu.core.movie.Movie
import com.dnegu.movieapp.common.BaseViewModel
import com.dnegu.movieapp.common.Error
import com.dnegu.movieapp.common.Success

class MovieListRecommendationsViewModel (private val getMovieList: GetMovieListRecommendations):
    BaseViewModel<List<Movie>, Any>() {
        fun getAll(id: Int, spanish: Boolean, id_recommendation: String) = executeUseCase {
            getMovieList(id, spanish, id_recommendation)
                .onSuccess { state.value = Success(it) }
                .onFailure { state.value = Error(it.throwable) }
        }
}