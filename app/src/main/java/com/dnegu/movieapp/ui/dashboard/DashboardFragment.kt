package com.dnegu.movieapp.ui.dashboard

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.dnegu.movieapp.R
import com.dnegu.core.movie.Movie
import com.dnegu.core.movie.MovieTrailer
import com.dnegu.data.common.PREF_ID_RECOMMENDATION
import com.dnegu.data.utils.LocalStorage
import com.dnegu.movieapp.common.Error
import com.dnegu.movieapp.common.Loading
import com.dnegu.movieapp.common.NoInternetState
import com.dnegu.movieapp.common.Success
import com.google.mlkit.common.model.DownloadConditions
import com.google.mlkit.nl.translate.TranslateLanguage
import com.google.mlkit.nl.translate.Translation
import com.google.mlkit.nl.translate.Translator
import com.google.mlkit.nl.translate.TranslatorOptions
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.emptyView
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.viewModel


class DashboardFragment : Fragment() {
    private lateinit var dashboardViewModel: DashboardViewModel
    private val movieTrailerViewModel: MovieTrailerViewModel by viewModel()
    private lateinit var movie: Movie
    private lateinit var movieTrailer: List<MovieTrailer>
    private lateinit var localStorage: LocalStorage

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)

        localStorage = LocalStorage(requireContext())
        movieTrailer = listOf()

        movie = try {
            requireArguments().getSerializable("movie") as Movie
        } catch (i:IllegalStateException){
            Movie()
        }

        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        movieTrailerViewModel.getAll(movie.id.toString())

        movieTrailerViewModel.viewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Loading -> emptyView.visibility = View.VISIBLE
                is Success -> {
                    if (viewState.data.isNotEmpty()) {
                        emptyView.visibility = View.GONE
                        showMoviesTrailer(viewState.data)
                    }
                }
                is Error -> {
                    Toast.makeText(context, viewState.error.message, Toast.LENGTH_SHORT).show()
                }
                is NoInternetState -> {
                    Toast.makeText(context, "Offline", Toast.LENGTH_SHORT).show()
                }
            }
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(movie.id == 0)
            mostrarVacio()
        else
            llenarDatos()

        btnBack.setOnClickListener {
            findNavController().navigate(R.id.action_navigation_dashboard_to_navigation_home)
        }

        btnShowTrailer.setOnClickListener {
            if(movieTrailer.isEmpty())
                Toast.makeText(requireContext(), "No se encontro trailer", Toast.LENGTH_SHORT).show()
            else{
                val movieT = movieTrailer.find{it.type == "Trailer"}
                if(movieT != null){
                    val url = "https://www.youtube.com/watch?v="+movieT.key
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.setPackage("com.google.android.youtube")
                    startActivity(intent)
                } else
                    Toast.makeText(requireContext(), "No se encontro trailer", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun showMoviesTrailer(data: List<MovieTrailer>) {
        movieTrailer = data
    }

    private fun mostrarVacio(){
        fullConstraint.visibility = View.GONE
        emptyConstraint.visibility = View.VISIBLE
    }

    private fun llenarDatos(){
        localStorage[PREF_ID_RECOMMENDATION] = movie.id.toString()

        fullConstraint.visibility = View.VISIBLE
        emptyConstraint.visibility = View.GONE

        movie.title?.let {
            lblTitleCentral.text = "\"$it\""
        }
        movie.release_date?.let {
            if (it.length > 4)
                lblDate.text = it.substring(0,4)
            else
                lblDate.text = it
        }
        movie.original_language?.let {
            lblLenguaje.text = it.toUpperCase()
        }
        movie.vote_average?.let {
            lblPuntaje.text = "$it"
        }
        movie.overview?.let {
            lblOverwiev.text = it
        }

        movie.poster_path?.let {
            if(it.substring(it.length-4) != "null")
                Glide.with(requireContext())
                    .load(it)
                    .error(R.drawable.ic_logo_png)
                    .into(imageView)
        }

        val options = TranslatorOptions.Builder()
            .setSourceLanguage(TranslateLanguage.ENGLISH)
            .setTargetLanguage(TranslateLanguage.SPANISH)
            .build()
        val englishSpanishTranslator: Translator = Translation.getClient(options)
        val conditions = DownloadConditions.Builder()
            .requireWifi()
            .build()
        englishSpanishTranslator.downloadModelIfNeeded(conditions)
            .addOnSuccessListener {
                englishSpanishTranslator.translate(movie.overview!!)
                    .addOnSuccessListener { translatedText ->
                        if(lblOverwiev2 != null)
                            lblOverwiev2.text = translatedText
                    }
                    .addOnFailureListener { exception ->
                        lblOverwiev2.text = getString(R.string.no_se_puede_traducir)
                    }
            }
            .addOnFailureListener { exception ->
                lblOverwiev2.text = getString(R.string.error_al_traducir)
            }

    }
}
