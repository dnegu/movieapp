package com.dnegu.movieapp.ui.home

import com.dnegu.core.common.onFailure
import com.dnegu.core.common.onSuccess
import com.dnegu.core.movie.GetMovieList
import com.dnegu.core.movie.GetMovieListTrending
import com.dnegu.core.movie.Movie
import com.dnegu.movieapp.common.BaseViewModel
import com.dnegu.movieapp.common.Error
import com.dnegu.movieapp.common.Success

class MovieListTrendingViewModel (private val getMovieList: GetMovieListTrending):
    BaseViewModel<List<Movie>, Any>() {
        fun getAll(id: Int) = executeUseCase {
            getMovieList(id)
                .onSuccess { state.value = Success(it) }
                .onFailure { state.value = Error(it.throwable) }
        }
}