package com.dnegu.movieapp.ui.dashboard

import com.dnegu.core.common.onFailure
import com.dnegu.core.common.onSuccess
import com.dnegu.core.movie.*
import com.dnegu.movieapp.common.BaseViewModel
import com.dnegu.movieapp.common.Error
import com.dnegu.movieapp.common.Success

class MovieTrailerViewModel (private val getMovieList: GetMovieTrailer):
    BaseViewModel<List<MovieTrailer>, Any>() {
        fun getAll(id: String) = executeUseCase {
            getMovieList(id)
                .onSuccess { state.value = Success(it) }
                .onFailure { state.value = Error(it.throwable) }
        }
}