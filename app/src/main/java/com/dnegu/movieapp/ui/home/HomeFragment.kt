package com.dnegu.movieapp.ui.home

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dnegu.movieapp.R
import com.dnegu.movieapp.common.*
import com.dnegu.movieapp.databinding.FragmentHomeBinding
import com.dnegu.core.movie.Movie
import com.dnegu.data.common.FIRSTPAGE
import com.dnegu.data.common.ID_RECOMMENDATION
import com.dnegu.data.common.LASTPAGE
import com.dnegu.data.common.PREF_ID_RECOMMENDATION
import com.dnegu.data.utils.LocalStorage
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(), CellClickListener {

    private var btnSpanishClicked = false
    private var btnTrendsClicked = false
    private lateinit var binding: FragmentHomeBinding
    private val viewModelMovie: MovieListViewModel by viewModel()
    private val viewModelMovieTrending: MovieListTrendingViewModel by viewModel()
    private val viewModelMovieRecommendations: MovieListRecommendationsViewModel by viewModel()
    private lateinit var localStorage: LocalStorage

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewmodel = viewModelMovie

        localStorage = LocalStorage(requireContext())

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeToData()
        setupList()

        var id = FIRSTPAGE
        viewModelMovie.getAll(id)
        viewModelMovieTrending.getAll(id)
        viewModelMovieRecommendations.getAll(id,false,
            localStorage[PREF_ID_RECOMMENDATION, ID_RECOMMENDATION]!!)

        //Pagination
        btnAdd.setOnClickListener {
            id = nextPage(id)
            viewModelMovie.getAll(id)
        }

        btnMinus.setOnClickListener {
            id = prevPage(id)
            viewModelMovie.getAll(id)
        }

        btnSpanish.setOnClickListener {
            if (!btnSpanishClicked){
                viewModelMovieRecommendations.getAll(id, true, localStorage[PREF_ID_RECOMMENDATION, ID_RECOMMENDATION]!!)
                btnSpanish.setBackgroundColor(Color.parseColor("#ffffff"))
                btnSpanish.setTextColor(Color.parseColor("#000000"))
                btnTrendsClicked = false
                btnTrend1993.setBackgroundColor(Color.parseColor("#000000"))
                btnTrend1993.setTextColor(Color.parseColor("#ffffff"))
            } else{
                viewModelMovieRecommendations.getAll(id, false, localStorage[PREF_ID_RECOMMENDATION, ID_RECOMMENDATION]!!)
                btnSpanish.setBackgroundColor(Color.parseColor("#000000"))
                btnSpanish.setTextColor(Color.parseColor("#ffffff"))
            }
            btnSpanishClicked = !btnSpanishClicked
        }

        btnTrend1993.setOnClickListener {
            if (!btnTrendsClicked){
                viewModelMovieRecommendations.getAll(id, true, localStorage[PREF_ID_RECOMMENDATION, ID_RECOMMENDATION]!!)
                btnTrend1993.setBackgroundColor(Color.parseColor("#ffffff"))
                btnTrend1993.setTextColor(Color.parseColor("#000000"))
                btnSpanishClicked = false
                btnSpanish.setBackgroundColor(Color.parseColor("#000000"))
                btnSpanish.setTextColor(Color.parseColor("#ffffff"))
            } else{
                viewModelMovieRecommendations.getAll(id, false, localStorage[PREF_ID_RECOMMENDATION, ID_RECOMMENDATION]!!)
                btnTrend1993.setBackgroundColor(Color.parseColor("#000000"))
                btnTrend1993.setTextColor(Color.parseColor("#ffffff"))
            }
            btnTrendsClicked = !btnTrendsClicked
        }
    }

    private fun subscribeToData() {
        viewModelMovie.viewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Loading -> emptyView.visibility = View.VISIBLE
                is Success -> {
                    if (viewState.data.isNotEmpty()) {
                        emptyView.visibility = View.GONE
                        showMoviesList(viewState.data)
                    }
                }
                is Error -> {
                    Toast.makeText(context, viewState.error.message, Toast.LENGTH_SHORT).show()
                }
                is NoInternetState -> {
                    Toast.makeText(context, "Offline", Toast.LENGTH_SHORT).show()
                }
            }
        }

        viewModelMovieTrending.viewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Loading -> emptyView.visibility = View.VISIBLE
                is Success -> {
                    if (viewState.data.isNotEmpty()) {
                        emptyView.visibility = View.GONE
                        showMoviesListTrending(viewState.data)
                    }
                }
                is Error -> {
                    Toast.makeText(context, viewState.error.message, Toast.LENGTH_SHORT).show()
                }
                is NoInternetState -> {
                    Toast.makeText(context, "Offline", Toast.LENGTH_SHORT).show()
                }
            }
        }

        viewModelMovieRecommendations.viewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Loading -> emptyView.visibility = View.VISIBLE
                is Success -> {
                    if (viewState.data.isNotEmpty()) {
                        emptyView.visibility = View.GONE
                        showMoviesListRecommendations(viewState.data)
                    }
                }
                is Error -> {
                    Toast.makeText(context, viewState.error.message, Toast.LENGTH_SHORT).show()
                }
                is NoInternetState -> {
                    Toast.makeText(context, "Offline", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setupList() {
        val divider = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        val linearLayoutManager = GridLayoutManager(context,2)
        rvWorkers.layoutManager = linearLayoutManager
        rvWorkers.addItemDecoration(divider)
        rvWorkers.adapter = GenericAdapter(this)

        val divider2 = DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL)
        val linearLayoutManager2 = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        rvUpcoming.layoutManager = linearLayoutManager2
        rvUpcoming.addItemDecoration(divider2)
        rvUpcoming.adapter = GenericAdapter(this)

        val divider3 = DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL)
        val linearLayoutManager3 = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        rvTrend.layoutManager = linearLayoutManager3
        rvTrend.addItemDecoration(divider3)
        rvTrend.adapter = GenericAdapter(this)
    }

    private fun showMoviesList(data: List<Movie>) {
        val adapter2 = GenericAdapter(this,true)
        rvUpcoming.adapter = adapter2
        adapter2.setItems(data)
    }

    private fun showMoviesListTrending(data: List<Movie>) {

        val adapter3 = GenericAdapter(this,true)
        rvTrend.adapter = adapter3
        adapter3.setItems(data)
    }

    private fun showMoviesListRecommendations(data: List<Movie>) {
        val adapter = GenericAdapter(this,true)
        rvWorkers.adapter = adapter
        if(data.size > 6)
            adapter.setItems(data.subList(0,6))
        else
            adapter.setItems(data)
    }

    private fun nextPage(id: Int): Int{
        return if(id == LASTPAGE) LASTPAGE
        else id+1
    }

    private fun prevPage(id: Int): Int{
        return if(id == FIRSTPAGE) FIRSTPAGE
        else id-1
    }

    override fun onCellClickListener(movie: Movie) {
        val bundle = Bundle()
        bundle.putSerializable("movie", movie)
        findNavController().navigate(R.id.action_navigation_home_to_navigation_dashboard,bundle)
    }
}