package com.dnegu.movieapp.di

import com.dnegu.movieapp.ui.dashboard.MovieTrailerViewModel
import com.dnegu.movieapp.ui.home.MovieListRecommendationsViewModel
import com.dnegu.movieapp.ui.home.MovieListTrendingViewModel
import com.dnegu.movieapp.ui.home.MovieListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    viewModel { MovieListViewModel(get()) }
    viewModel { MovieListTrendingViewModel(get()) }
    viewModel { MovieListRecommendationsViewModel(get()) }
    viewModel { MovieTrailerViewModel(get()) }
}