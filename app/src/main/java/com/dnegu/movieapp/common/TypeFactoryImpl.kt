package com.dnegu.movieapp.common

import com.dnegu.movieapp.R
import com.dnegu.core.common.TypeFactory
import com.dnegu.core.movie.Movie

class TypeFactoryImpl : TypeFactory {
    override fun type(item: Movie): Int {
        return R.layout.row_movie
    }

    override fun typeHorizontal(item: Movie): Int {
        return R.layout.row_movie_horizontal
    }
}