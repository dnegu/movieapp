package com.dnegu.movieapp.common

import com.dnegu.core.movie.Movie

interface CellClickListener {
    fun onCellClickListener(movie: Movie)
}