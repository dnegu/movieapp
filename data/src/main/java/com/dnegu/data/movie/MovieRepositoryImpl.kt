package com.dnegu.data.movie

import com.dnegu.core.common.Result
import com.dnegu.core.movie.Movie
import com.dnegu.core.movie.MovieRepository
import com.dnegu.core.movie.MovieTrailer
import com.dnegu.data.common.Repository
import com.dnegu.data.network.Api
import com.dnegu.data.network.getData
import com.dnegu.data.network.getDataAsListApi

class MovieRepositoryImpl(private val api: Api, private val movieDao: MovieDao):
    Repository<Movie, MovieEntity>(), MovieRepository {
    private val api_keyValue = "f46b58478f489737ad5a4651a4b25079"

    override suspend fun getMovieList(id: Int): Result<List<Movie>> {
        return fetchDataList(
            apiDataProvider = {
                val datos = api.getMovieList(id,api_keyValue)
                datos.body()?.results?.map { it.mapToRoomEntity() }
                    ?.let { movieDao.insertMultiple(it) }
                datos.getDataAsListApi()
            },
            dbDataProvider = {
                movieDao.getMovieList()
            }
        )
    }

    override suspend fun getMovieListTrending(id: Int): Result<List<Movie>> {
        return fetchDataList(
            apiDataProvider = {
                val datos = api.getMovieListTrending(id,api_keyValue)
                datos.body()?.results?.map { it.mapToRoomEntity() }
                    ?.let { movieDao.insertMultiple(it) }
                datos.getDataAsListApi()
            },
            dbDataProvider = {
                movieDao.getMovieList()
            }
        )
    }

    override suspend fun getMovieListRecommendations(id: Int, spanish: Boolean,id_recommendation: String): Result<List<Movie>> {
        val languageES = "es-PE"
        val languageEN = "en-US"
        val lang = if (spanish) languageES else languageEN
        return fetchDataList(
            apiDataProvider = {
                val datos = api.getMovieListRecommendations(id_recommendation,id,api_keyValue, lang )
                datos.body()?.results?.map { it.mapToRoomEntity() }
                    ?.let { movieDao.insertMultiple(it) }
                datos.getDataAsListApi()
            },
            dbDataProvider = {
                movieDao.getMovieList()
            }
        )
    }

    override suspend fun getMovieById(id: Int): Result<Movie> {
        return fetchData(
            apiDataProvider = {
                api.getMovieById(id).getData(
                    fetchFromCacheAction = { movieDao.getMovieById(id) },
                    cacheAction = { movieDao.insert(it) }
                )
            },
            dbDataProvider = {
                movieDao.getMovieById(id)
            }
        )
    }
}