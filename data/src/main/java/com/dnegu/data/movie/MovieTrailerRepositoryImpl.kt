package com.dnegu.data.movie

import com.dnegu.core.common.Result
import com.dnegu.core.movie.MovieTrailer
import com.dnegu.core.movie.MovieTrailerRepository
import com.dnegu.data.common.Repository
import com.dnegu.data.network.Api
import com.dnegu.data.network.getDataAsListApi

class MovieTrailerRepositoryImpl(private val api: Api, private val movieDao: MovieTrailerDao):
    Repository<MovieTrailer, MovieTrailerEntity>(), MovieTrailerRepository {
    private val API_KEY_THE_MOVIE = "f46b58478f489737ad5a4651a4b25079"

    override suspend fun getMovieTrailer(id_recommendation: String): Result<List<MovieTrailer>> {
        val languageES = "en-US"
        return fetchDataList(
            apiDataProvider = {
                val datos = api.getMovieTrailer(id_recommendation,API_KEY_THE_MOVIE, languageES )
                datos.body()?.results?.map { it.mapToRoomEntity() }
                    ?.let { movieDao.insertMultiple(it) }
                datos.getDataAsListApi()
            },
            dbDataProvider = {
                //movieDao.getMovieList()
                listOf<MovieTrailerEntity>()
            }
        )
    }

}