package com.dnegu.data.movie

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dnegu.core.movie.Movie
import com.dnegu.core.movie.MovieTrailer
import com.dnegu.data.network.DomainMapper

@Entity(tableName = "movie_trailer")
data class MovieTrailerEntity (
    @PrimaryKey
    val id: String,
    val iso_3166_1: String,
    val iso_639_1: String,
    val key: String,
    val name: String,
    val official: Boolean,
    val published_at: String,
    val site: String,
    val size: Int,
    val type: String
) : DomainMapper<MovieTrailer> {
    override fun mapToDomainModel(): MovieTrailer {
        return MovieTrailer(id,iso_3166_1,iso_639_1,key,name,official,published_at,site,size,type)
    }
}