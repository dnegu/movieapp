package com.dnegu.data.movie

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dnegu.core.movie.MovieTrailer

@Dao
interface MovieTrailerDao {

    @Query("select * from movie_trailer")
    suspend fun getMovieList(): List<MovieTrailerEntity>

    @Query("select * from movie_trailer where id = :id")
    suspend fun getMovieById(id: Int): MovieTrailerEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movieEntity: MovieTrailerEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMultiple(movieList: List<MovieTrailerEntity>)
}