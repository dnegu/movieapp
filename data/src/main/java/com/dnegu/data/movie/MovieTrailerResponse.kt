package com.dnegu.data.movie

import com.dnegu.core.movie.MovieTrailer
import com.dnegu.data.network.RoomMapper

data class MovieTrailerResponse(
    val id: String,
    val iso_3166_1: String,
    val iso_639_1: String,
    val key: String,
    val name: String,
    val official: Boolean,
    val published_at: String,
    val site: String,
    val size: Int,
    val type: String
): RoomMapper<MovieTrailerEntity> {
    override fun mapToRoomEntity(): MovieTrailerEntity {
        return MovieTrailerEntity(id,iso_3166_1,iso_639_1,key,name,official,published_at,site,size,type)
    }
}