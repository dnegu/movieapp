package com.dnegu.data.common

const val DB_NAME = "database.sqlite"
const val DB_ENTRY_ERROR = "No entry found in database"
const val GENERAL_NETWORK_ERROR = "Something went wrong, please try again."
const val FIRSTPAGE = 1
const val LASTPAGE = 14
const val ID_RECOMMENDATION = "634649"
const val PREF_API_TOKEN = "pref_api_token"
const val PREF_ID_RECOMMENDATION = "pref_api_token"