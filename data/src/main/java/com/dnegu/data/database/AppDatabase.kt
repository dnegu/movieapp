package com.dnegu.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dnegu.data.movie.MovieDao
import com.dnegu.data.movie.MovieEntity
import com.dnegu.data.movie.MovieTrailerDao
import com.dnegu.data.movie.MovieTrailerEntity

@Database(entities = [MovieEntity::class, MovieTrailerEntity::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun movieTrailerDao(): MovieTrailerDao
}