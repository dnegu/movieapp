package com.dnegu.data.network

import com.dnegu.data.login.LoginRequest
import com.dnegu.data.login.LoginResponse
import com.dnegu.data.movie.ApiResponse
import com.dnegu.data.movie.MovieResponse
import com.dnegu.data.movie.MovieTrailerResponse
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @POST("auth")
    suspend fun authenticate(@Body request: LoginRequest): Response<LoginResponse>

    @GET("movie/upcoming")
    suspend fun getMovieList(@Query("page") page: Int,
                             @Query("api_key") api_key: String): Response<ApiResponse<MovieResponse>>

    @GET("trending/all/day")
    suspend fun getMovieListTrending(@Query("page") page: Int,
                             @Query("api_key") api_key: String): Response<ApiResponse<MovieResponse>>

    @GET("movie/{id_recommendation}/recommendations")
    suspend fun getMovieListRecommendations(
                            @Path("id_recommendation") id: String,
                            @Query("page") page: Int,
                             @Query("api_key") api_key: String,
                             @Query("language") language: String
                             ): Response<ApiResponse<MovieResponse>>

    @GET("movie/{id_recommendation}/videos")
    suspend fun getMovieTrailer(
                            @Path("id_recommendation") id: String,
                             @Query("api_key") api_key: String,
                             @Query("language") language: String
                             ): Response<ApiResponse<MovieTrailerResponse>>

    @GET("worker/{id}")
    suspend fun getMovieById(@Path("id") id: Int): Response<MovieResponse>
}