package com.dnegu.core.movie

import com.dnegu.core.common.Result

interface GetMovieListTrending{
    suspend operator fun invoke(id: Int): Result<List<Movie>>
}

class GetMovieListTrendingImpl(private val movieRepository: MovieRepository): GetMovieListTrending{
    override suspend fun invoke(id: Int): Result<List<Movie>> {
        return movieRepository.getMovieListTrending(id)
    }
}