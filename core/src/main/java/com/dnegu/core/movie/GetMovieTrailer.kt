package com.dnegu.core.movie

import com.dnegu.core.common.Result

interface GetMovieTrailer{
    suspend operator fun invoke(id: String): Result<List<MovieTrailer>>
}

class GetMovieTrailerImpl(private val movieRepository: MovieTrailerRepository): GetMovieTrailer{
    override suspend fun invoke(id: String): Result<List<MovieTrailer>> {
        return movieRepository.getMovieTrailer(id)
    }
}