package com.dnegu.core.movie

import com.dnegu.core.common.Result

interface GetMovieListRecommendations{
    suspend operator fun invoke(id: Int, spanish: Boolean, id_recommendation: String): Result<List<Movie>>
}

class GetMovieListRecommendationsImpl(private val movieRepository: MovieRepository): GetMovieListRecommendations{
    override suspend fun invoke(id: Int, spanish: Boolean, id_recommendation: String): Result<List<Movie>> {
        return movieRepository.getMovieListRecommendations(id,spanish,id_recommendation)
    }
}