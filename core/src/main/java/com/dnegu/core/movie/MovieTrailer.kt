package com.dnegu.core.movie

import com.dnegu.core.common.TypeFactoryTrailer
import com.dnegu.core.common.VisitableTrailer
import java.io.Serializable


data class MovieTrailer (
    val id: String,
    val iso_3166_1: String,
    val iso_639_1: String,
    val key: String,
    val name: String,
    val official: Boolean,
    val published_at: String,
    val site: String,
    val size: Int,
    val type: String
): VisitableTrailer, Serializable {
    override fun type(typeFactory: TypeFactoryTrailer): Int {
        return typeFactory.type(this)
    }

    override fun typeHorizontal(typeFactory: TypeFactoryTrailer): Int {
        return typeFactory.typeHorizontal(this)
    }
}