package com.dnegu.core.movie

import com.dnegu.core.common.Result

interface MovieTrailerRepository {
    suspend fun getMovieTrailer(id_recommendation: String): Result<List<MovieTrailer>>
}