package com.dnegu.core.di

import com.dnegu.core.login.Authenticate
import com.dnegu.core.login.AuthenticateImpl
import com.dnegu.core.movie.*
import org.koin.dsl.module

val interactionModule = module {

    factory<Authenticate> { AuthenticateImpl(get()) }

    factory<GetMovieList> { GetMovieListImpl(get()) }

    factory<GetMovieListTrending> { GetMovieListTrendingImpl(get()) }

    factory<GetMovieListRecommendations> { GetMovieListRecommendationsImpl(get()) }

    factory<GetMovieTrailer> { GetMovieTrailerImpl(get()) }
}