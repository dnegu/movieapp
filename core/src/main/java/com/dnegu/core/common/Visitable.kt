package com.dnegu.core.common

interface Visitable {
    fun type(typeFactory: TypeFactory): Int
    fun typeHorizontal(typeFactory: TypeFactory): Int
}

interface VisitableTrailer {
    fun type(typeFactory: TypeFactoryTrailer): Int
    fun typeHorizontal(typeFactory: TypeFactoryTrailer): Int
}