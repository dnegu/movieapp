package com.dnegu.core.common

import com.dnegu.core.movie.Movie
import com.dnegu.core.movie.MovieTrailer

interface TypeFactory {
    fun type(item: Movie): Int
    fun typeHorizontal(item: Movie):Int
}

interface TypeFactoryTrailer {
    fun type(item: MovieTrailer): Int
    fun typeHorizontal(item: MovieTrailer):Int
}